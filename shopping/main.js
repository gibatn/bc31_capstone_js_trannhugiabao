import { localStoreController } from "./ProductController/localStore.js";
import { Products } from "./ProductModel/productModel.js";
import { productService } from "./ProductService/productService.js";
let productList = [];

let renderProduct = (list) => {
  let product = list.map((item) => {
    return ` <div class="card col-3" style="width: 18rem">
    <img class="card-img-top pt-3" src=${item.img} alt="Card image cap" />
    <div class="card-body" >
      <h5 class="card-title">${item.name}</h5>
      <p class="card-text"  style="min-height: 3.5rem">
       ${item.desc}
      </p>
     <div class="d-flex">
     <span class="mr-auto p-2">$${item.price}</span>
     <button onclick="addToCart('${item.id}')" class="btn btn-primary">
              Add to cart
            </button>
     
     </div>
    </div>
  </div>`;
  });
  document.getElementById("layout-product").innerHTML = product.join(" ");
};

let renderProductService = () => {
  productService
    .getProduct()
    .then((result) => {
      productList = result.data;
      renderProduct(productList);
    })
    .catch((error) => {});
};

renderProductService();
let cartArr = [];
let newItem = "";

let addToCart = (id) => {
  let indexCart = cartArr.findIndex((item) => item.id == id);
  let index = productList.findIndex((item) => item.id == id);
  if (indexCart === -1) {
    newItem = { ...productList[index], quantity: 1 };
    cartArr.push(newItem);
  } else {
    cartArr[indexCart].quantity += 1;
  }
  cart();
  // quanti += cartArr[index].quantity;
  //
  //

  // document.getElementById("cart-number").innerHTML = `${quanti}`;
};
window.addToCart = addToCart;

let cart = () => {
  localStoreController.setStore(cartArr, "cartProduct");
  // setStore(cartArr, "cartProduct");
  let cartQuantity = cartArr.reduce((st, item) => {
    return (st += item.quantity);
  }, 0);
  document.getElementById("cart-number").innerHTML = cartQuantity;
  let renderCart = cartArr.map((item, index) => {
    return `<tr>
    <td> <img style="width:50px" class="card-img-top" src=${
      item.img
    } alt="Card image cap" /></td>
     <td>${item.name}</td> 
     <td>
     <i onclick="changeQuantity('${item.id}',-1)" class="fa fa-angle-left"></i>
     <span>${item.quantity}</span>
     <i onclick="changeQuantity('${item.id}',1)" class="fa fa-angle-right"></i>

     </td>
     <td>$${item.price * item.quantity}</td>
     <td>
   <button class="btn btn-danger" onclick="deleteCart('${
     item.id
   }')"> <i   class="fa fa-trash"></i></button>
     </td>
     </tr>`;
  });
  renderCart = renderCart.join(" ");

  let cartmodal = `
  <div >
  <table class="table" style="height:500px">
    <tbody>
      ${renderCart}
    </tbody>
  </table>

  <div class="text-right pr-4 pt-5">
    <div class="cart-bottom">
      <strong>Tổng tiền:</strong>
      <span>
        $${cartArr.reduce(
          (tt, item) => (tt += item.price * item.quantity),
          0
        )}</span
      >

      <button onClick="deleteAllCart('Mua')">
        Purchase
        <i class="fa fa-money-check-alt"></i>
      </button>

      <button onClick="deleteAllCart('Xóa')">
        Clear Cart
        <i class="fa fa-trash"></i>
      </button>
    </div>
  </div>
</div>
    `;

  cartArr.length == 0
    ? (document.getElementById("cart-item").innerHTML =
        "Looks Like You Haven't Added Any Product In The Cart")
    : (document.getElementById("cart-item").innerHTML = cartmodal);
};

let changeQuantity = (id, num) => {
  let index = cartArr.findIndex((item) => item.id == id);
  cartArr[index].quantity += num;
  if (cartArr[index].quantity <= 0) {
    cartArr.splice(index, 1);
  }
  cart();
};
window.changeQuantity = changeQuantity;
let deleteCart = (id) => {
  let index = cartArr.findIndex((item) => item.id == id);
  if (index !== -1) {
    cartArr.splice(index, 1);
  }
  cart(id);
};
window.deleteCart = deleteCart;
let deleteAllCart = (text) => {
  let result = confirm(`Bạn có muốn ${text}?`);
  if (result == true) {
    alert(`${text} thành công`);
    cartArr = [];
    cart();
  } else {
    alert("Mua sắm tiếp nào");
  }
};
window.deleteAllCart = deleteAllCart;
let search = (value) => {
  let searchArr = [];
  searchArr = productList.filter(
    (item) => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
  );
  renderProduct(searchArr);
};
window.search = search;

// function setStore(content, storeName) {
//   let sContent = JSON.stringify(content);
//   localStorage.setItem(storeName, sContent);
// }
// function getStore(storeName) {
//   let output;
//   if (localStorage.getItem(storeName)) {
//     output = JSON.parse(localStorage.getItem(storeName));
//   }
//   return output;
// }
window.onload = function () {
  let content = localStoreController.getStore("cartProduct");
  if (content) {
    cartArr = content;
    cart();
  }
};

console.log("heheh");
