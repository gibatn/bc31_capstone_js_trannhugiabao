import { adminController } from "./Controller/adminController.js";
import { adminService } from "./Service/adminService.js";
import { showhideBtn } from "./Service/showHideBtn.js";
import { validation } from "./Service/validation.js";

let productArr = [];
let idUpdate = "";
let renderTable = (listProduct) => {
  let product = listProduct.map((item) => {
    return `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>
  <img width=50 src='${item.img}' alt="">
        
        </td>
        <td>${item.desc}</td>
        <td>
        <button onClick =deleteProduct('${item.id}') class="btn btn-danger">Xóa</button>
        <button onClick = showThongTinLenForm('${item.id}') class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Xem</button>
        </td>
        </tr>`;
  });
  document.getElementById("tbody").innerHTML = product.join(" ");
};
let renderService = () => {
  adminService
    .getProduct()
    .then((res) => {
      productArr = res.data;
      renderTable(productArr);
    })

    .catch((err) => {});
};
renderService();
let addProdduct = () => {
  let Phone = adminController.layThongTinTuForm();

  let isValid = true;
  isValid &= validation.kTRong(Phone.name, "tbTen", "tên");
  if (isValid) {
    adminService
      .addProduct(Phone)
      .then((result) => {
        document.getElementById("myForm").reset();

        renderService();
      })
      .catch((error) => {});
  }
};
window.addProdduct = addProdduct;
let deleteProduct = (id) => {
  adminService
    .deleteProduct(id)
    .then((result) => {
      renderService();
    })
    .catch((error) => {});
};
window.deleteProduct = deleteProduct;
let showThongTinLenForm = (idPhone) => {
  idUpdate = idPhone;

  adminService
    .getInfoProduct(idPhone)
    .then((result) => {
      showhideBtn.showUpdate();
      adminController.showThongTinLenForm(result.data);
    })
    .catch((error) => {});
};
window.showThongTinLenForm = showThongTinLenForm;
let updateProduct = () => {
  let phone = adminController.layThongTinTuForm();
  let newPhone = { ...phone, id: idUpdate };
  adminService
    .updateProduct(newPhone)
    .then((result) => {
      document.getElementById("myForm").reset();
      showhideBtn.hideUpdate();
      renderService();
    })
    .catch((error) => {});
};
window.updateProduct = updateProduct;
let timKiemSP = () => {
  let tt = document.getElementById("txt-tim-kiem").value;

  var SearchArr = [];
  SearchArr = productArr.filter((sp) => {
    return sp.name.toLowerCase().indexOf(tt.toLowerCase()) > -1;
  });
  renderTable(SearchArr);
};
window.timKiemSP = timKiemSP;
