export let validation = {
  kTRong: (value, id, message) => {
    let str = value.trim();
    if (str.length > 0) {
      document.getElementById(id).innerHTML = "";
      return true;
    } else {
      document.getElementById(id).innerHTML = `Vui lòng nhập ${message}`;
      return false;
    }
  },
};
