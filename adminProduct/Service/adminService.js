export let adminService = {
  getProduct: () => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/product",
      method: "GET",
    });
  },
  addProduct: (phone) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/product",
      method: "POST",
      data: phone,
    });
  },
  deleteProduct: (id) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/product/" + id,
      method: "DELETE",
    });
  },
  getInfoProduct: (id) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/product/" + id,
      method: "GET",
    });
  },
  updateProduct: (phone) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/product/" + phone.id,
      method: "PUT",
      data: phone,
    });
  },
};
