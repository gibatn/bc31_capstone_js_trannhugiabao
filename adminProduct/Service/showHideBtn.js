export let showhideBtn = {
  showUpdate: () => {
    document.getElementById("btnCapNhat").style.display = "block";
    document.getElementById("btnThemMoi").style.display = "none";
  },
  hideUpdate: () => {
    document.getElementById("btnCapNhat").style.display = "none";
    document.getElementById("btnThemMoi").style.display = "block";
    document.getElementById("btnCapNhat").setAttribute("data-dismiss", "modal");
  },
};
