export let adminController = {
  layThongTinTuForm: () => {
    let ten = document.getElementById("txt-ten").value;
    let gia = document.getElementById("txt-gia").value;
    let hinhAnh = document.getElementById("txt-hinh-anh").value;
    let moTa = document.getElementById("txt-mo-ta").value;
    let Phone = {
      name: ten,
      price: gia,
      image: hinhAnh,
      desc: moTa,
    };
    return Phone;
  },
  showThongTinLenForm: (res) => {
    document.getElementById("txt-ten").value = res.name;
    document.getElementById("txt-gia").value = res.price;
    document.getElementById("txt-hinh-anh").value = res.image;
    document.getElementById("txt-mo-ta").value = res.desc;
  },
};
